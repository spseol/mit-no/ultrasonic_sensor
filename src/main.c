#include <stdbool.h>
#include <stm8s.h>
#include <stdio.h>
#include "main.h"
#include "milis.h"
#include "delay.h"
#include "uart1.h"

void init(void)
{
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);      // taktovani MCU na 16MHz

    GPIO_Init(TRIGGER_PORT, TRIGGER_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(ECHO_PORT, ECHO_PIN, GPIO_MODE_IN_FL_NO_IT);

    init_milis();
    init_uart1();

    TIM2_TimeBaseInit(TIM2_PRESCALER_16, 0xFFFF);
    TIM2_Cmd(ENABLE);

}

uint16_t get_echo_time(void)
{
    uint16_t t1, t2;
    uint32_t time = milis();

    HIGH(TRIGGER);
    delay_us(1);
    LOW(TRIGGER);

    while (READ(ECHO) == 0) {
        // čekám ale musím kontrolovat jestli nedošlo k nějaké nepředloženosti a jestli
        // náhodou nečekám už nějak moc dlouho
        if (milis() - time > 65) {
            return 0xFFFF;
        }
    }
    t1 = TIM2_GetCounter();

    while (READ(ECHO) == 1)
        if (milis() - time > 65)
            return 0xFFFF;
    t2 = TIM2_GetCounter();

    return t2 - t1;
}

int main(void)
{

    uint32_t time = 0;
    uint16_t echotime;

    init();
    printf("Začínáme...\n");

    while (1) {
        if (milis() - time > 777) {
            time = milis();
            echotime = get_echo_time();
            printf("čas = %u µs  vzdálenost = %u mm\n", echotime,
                   (uint16_t) ((echotime * 343L + 1000) / 2000));
        }
        //delay_ms(333);
    }
}

/*-------------------------------  Assert -----------------------------------*/
#include "__assert__.h"
