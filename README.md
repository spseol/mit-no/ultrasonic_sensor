Ultrazvukový senzor vzdálenosti HC-SR04
=========================================

Datasheety jsou [jeden](datasheets/HC-SR04-Ultrasonic.pdf) lepší
než [druhý](datasheets/HCSR04.pdf)

![](datasheets/HC-SR04-Ultrasonic-Distance-Sensor-Pinout.webp)

Jak to celé funguje
-----------------------

![timing diagram](datasheets/timing.png)

1. Mikroprocesor vygeneruje na pinu TRIGGER krátký impulz, kterým senzoru řekne,
   aby zahájil měření.
2. Senzor na pinu ECHO vygeneruje impulz, který je přesně tak dlouhý, jako se zvuk 
   šíří od senzoru k překážce a zase zpět.

Schema zapojení
----------------------------

![]()


Stručná funkce programu
----------------------------

... no ...ono nám stačí vygenerovat spouštěcí impulz a změřit jak dlouho trvá ECHO
impulz. Celé řešení se tedy více/méně stojí na problému *jak změřím čas trvání ECHO
impulzu*. 

Měřit délku impulzu budeme časovačem. V tomto příkladu jsem použil TIM2. Předděličku
jsem si nastavil tak, aby jeden tik trval 1 µs.

### Tupé čekání na hranu.

Tento princip je poměrně jednoduchý a dá se zapsat do jediné blokující funkce.
Po vygenerování spouštěcího impulzu prostě tupě čekám na náběžnou hranu ECHO impulzu.
Při této hraně si zapíšu obsah registru časovače. Pak zase tupě čekám na sestupnou
hranu. Jakmile přijde, zase zapíšu obsah registru časovače a tyto dvě zapamatované
hodnoty od sebe odečtu. :) hotovo.

Toto řešení najdete ve funkci `get_echo_time()`.

### Měření pomocí pokročilých funkcí časovače.

Toto měření je neblokující. Vše okolo je potom řešeno technikou
["stavový automat"](http://www.elektromys.eu/clanky/stm8_12_automat/clanek.html).

Časovač nakonfiguruji tak, aby se jeho registr při vzestupné hraně přepsal do
registru `CC1` a při sestupné hraně do registru `CC2` (Capture/Compare
registry). Potom stačí od sebe odečíst `CC2` a `CC1` a mám čas trvání ECHO impulzu.
